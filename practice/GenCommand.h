﻿#pragma once

#include "Command.h"
#include "CurrentData.h"

namespace Practice 
{
	using namespace std;

	struct GenCommand : Command
	{
		CurrentData& dataInstance = CurrentData::Instance();

		int width, 
			height, 
			numberOfFiles, 
			minNumber, 
			maxNumber;

		void clean_stdin() {
			while (getchar() != '\n');
		}

		int readIntFromConsole() {
			int result;
			while (!(cin >> result)) {
				cin.clear();
				clean_stdin();
				cout << "Number incorrect. Please try again:\n";
			}
			return result;
		}

		void exec() 
		{
			cout << " --------------------------------------------------------------- " << endl;
			cout << " <Генерация массива> " << endl;
			cout << endl;
			cout << "     Введите необх. кол-во строк: ";
			width = readIntFromConsole();

			cout << "  Введите необх. кол-во столбцов: ";
			height = readIntFromConsole();

			cout << "   Минимальное значение элемента: ";
			minNumber = readIntFromConsole();

			cout << "  Максимальное значение элемента: ";
			maxNumber = readIntFromConsole();

				if (minNumber > maxNumber) 
				{
					cout << endl;
					cout << " Ошибка: Минимальное число должно быть < макс." << endl << endl;
					getchar();
					return;
					
				}

			cout << "               Количество файлов: ";
			numberOfFiles = readIntFromConsole();
			cout << endl;

			getchar();

			if (confirm()) 
			{
				generate();
			}
		}


		int randBetween(int from, int to) 
		{
			return rand() % (to - from + 1) + from;
		}

		void generate() 
		{
			dataInstance.width = width;
			dataInstance.height = height;
			dataInstance.numberOfArrays = numberOfFiles;

			dataInstance.matrices.clear();

			for (int i = 0; i < numberOfFiles; i++) 
			{
				MatrixInt matrix(generateVector(), width, height);
				dataInstance.matrices.push_back(matrix);
				ofstream outputFile(to_string(i + 1) + ".txt");
				outputFile << matrix.toString();
			}

			cout << " Генерация завершена." << endl;
			cout << " --------------------------------------------------------------- " << endl;
		}

		vector<vector<int>> generateVector() 
		{
			vector<vector<int>> result = {};
			for (int i = 0; i < height; i++) 
			{
				vector<int> line = {};
				for (int j = 0; j < width; j++) 
				{
				
					line.push_back(randBetween(minNumber, maxNumber));
					
				}
				result.push_back(line);
			}
			return result;
		}
	};
}